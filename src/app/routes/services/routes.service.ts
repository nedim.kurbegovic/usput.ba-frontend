import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Route } from '../interfaces/route.interface';
import { Filter } from '../interfaces/filter.interface';

@Injectable({
  providedIn: 'root',
})
export class RoutesService {
  private readonly URL = '/api/routes';

  constructor(private http: HttpClient) {}

  get(filter: Filter, page: number, size: number): Observable<Route[]> {
    let params = new HttpParams();

    params = params.set('page', page.toString());
    params = params.set('size', size.toString());

    if (filter.od) {
      params = params.set('od', filter.od.toString());
    }

    if (filter.do) {
      params = params.set('do', filter.do.toString());
    }

    if (filter.datum) {
      params = params.set('datum', filter.datum);
    }

    if (filter.voznje) {
      params = params.set('voznje', filter.voznje ? 'da' : 'ne');
    }

    if (filter.potraznje) {
      params = params.set('potraznje', filter.potraznje ? 'da' : 'ne');
    }

    return this.http.get<Route[]>(this.URL, { params });
  }
}
