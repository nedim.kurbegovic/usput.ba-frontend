export enum RouteStatusType {
  Active = 'ACTIVE',
  Completed = 'COMPLETED',
  Canceled = 'CANCELED'
}
