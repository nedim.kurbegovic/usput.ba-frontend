import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NgbDateStruct, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject } from 'rxjs';
import { Region } from '../../../shared/region.interface';
import { getRegions } from '../../../state/app.selectors';
import { Store } from '@ngrx/store';
import { loadRegionsAction } from '../../../state/app.actions';
import { ActivatedRoute, Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import {
  MAX_SEARCH_DATE,
  MIN_SEARCH_DATE,
} from '../../../shared/constants/constants';
import { Filter } from '../../interfaces/filter.interface';
import { faSlidersH } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-filter-desktop',
  templateUrl: './filter.component.html',
})
export class FilterComponent implements OnInit, OnDestroy {
  @Output() filtered = new EventEmitter<Filter>();

  form: FormGroup;
  faSliders = faSlidersH;
  minDate: NgbDateStruct = MIN_SEARCH_DATE;
  maxDate: NgbDateStruct = MAX_SEARCH_DATE;
  regions$: Observable<Region[]> = this.store.select(getRegions);
  private unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store,
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.formBuilder.group({
      od: null,
      do: null,
      datum: null,
      voznje: false,
      potraznje: false,
    });
    this.store.dispatch(loadRegionsAction());
  }

  ngOnInit(): void {
    this.observeFilterChange();
  }

  private observeFilterChange(): void {
    this.activatedRoute.queryParamMap
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((paramMap) => {
        this.form.patchValue({
          od: paramMap.get('od') ? Number(paramMap.get('od')) : null,
          do: paramMap.get('do') ? Number(paramMap.get('do')) : null,
          datum: paramMap.get('datum'),
          voznje: paramMap.get('voznje') === 'da',
          potraznje: paramMap.get('potraznje') === 'da',
        });

        this.filtered.emit(this.form.value);
      });
  }

  updateQueryParams(name: string, value: string | number | null): void {
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: { [name]: value },
      queryParamsHandling: 'merge',
    });
  }

  openMobileFilter(content: TemplateRef<any>): void {
    this.modalService.open(content, { centered: true });
  }

  closeModal(): void {
    this.modalService.dismissAll();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
