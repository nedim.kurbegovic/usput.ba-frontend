import { Component } from '@angular/core';
import { Filter } from './interfaces/filter.interface';
import { SEARCH_RESULTS_SIZE } from '../shared/constants/constants';
import { Store } from '@ngrx/store';
import { loadRoutesAction } from './store/routes.actions';
import { getRouteStateSelector } from './store/routes.selectors';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
})
export class RoutesComponent {
  page = 0;
  size = SEARCH_RESULTS_SIZE;
  routes$ = this.store.select(getRouteStateSelector);

  constructor(private store: Store) {}

  // Poziva se automatski pri učitavanju
  search(filter: Filter): void {
    this.store.dispatch(
      loadRoutesAction({ page: this.page, size: this.size, filter })
    );
  }
}
