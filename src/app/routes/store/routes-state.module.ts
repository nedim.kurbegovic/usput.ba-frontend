import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {routesFeatureKey} from './routes.reducer';
import * as fromRoutes from './routes.reducer';
import { EffectsModule } from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {RoutesEffects} from './routes.effects';

@NgModule({
  imports: [
    StoreModule.forFeature(routesFeatureKey, fromRoutes.reducer),
    EffectsModule.forFeature([RoutesEffects]),
    CommonModule
  ]
})
export class RoutesStateModule {}
