import { createAction, props } from '@ngrx/store';
import { Route } from '../interfaces/route.interface';
import { Filter } from '../interfaces/filter.interface';

export const loadRoutesAction = createAction(
  '[Routes Actions] Load Routes',
  props<{ filter: Filter; page: number; size: number }>()
);

export const loadRoutesSuccessAction = createAction(
  '[Routes Actions] Load Routes Success',
  props<{ routes: Route[] }>()
);

export const routesFailureAction = createAction(
  '[Routes Actions] Routes Failure',
  props<{ error: any }>()
);
