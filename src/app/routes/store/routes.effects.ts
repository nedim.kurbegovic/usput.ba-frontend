import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import {
  loadRoutesAction,
  loadRoutesSuccessAction,
  routesFailureAction,
} from './routes.actions';
import { catchError, map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { RoutesService } from '../services/routes.service';
import { of } from 'rxjs';

@Injectable()
export class RoutesEffects {
  loadRoutes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loadRoutesAction),
      switchMap((action) => {
        const { filter, page, size } = action;
        return this.service.get(filter, page, size).pipe(
          map((res) => loadRoutesSuccessAction({ routes: res })),
          catchError((error) => of(routesFailureAction({ error })))
        );
      })
    )
  );

  constructor(
    private actions$: Actions,
    private router: Router,
    private service: RoutesService
  ) {}
}
