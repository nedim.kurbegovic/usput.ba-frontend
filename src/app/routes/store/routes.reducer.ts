import { Action, createReducer, on } from '@ngrx/store';
import { Route } from '../interfaces/route.interface';
import { loadRoutesAction, loadRoutesSuccessAction } from './routes.actions';

export const routesFeatureKey = 'routes';

export interface RoutesState {
  routes: Route[];
  loading: boolean;
}

export const initialState: RoutesState = {
  routes: [],
  loading: false,
};

const routesReducer = createReducer<RoutesState>(
  initialState,
  on(loadRoutesAction, (state) => ({
    ...state,
    loading: true,
  })),
  on(loadRoutesSuccessAction, (state, { routes }) => ({
    ...state,
    routes,
    loading: false,
  }))
);

export function reducer(state: RoutesState, action: Action): RoutesState {
  return routesReducer(state, action);
}
