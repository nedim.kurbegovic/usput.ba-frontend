import { createFeatureSelector, createSelector } from '@ngrx/store';
import { routesFeatureKey, RoutesState } from './routes.reducer';

export const getRoutesState = createFeatureSelector<RoutesState>(
  routesFeatureKey
);

export const getRouteStateSelector = createSelector(
  getRoutesState,
  (state: RoutesState) => state
);
