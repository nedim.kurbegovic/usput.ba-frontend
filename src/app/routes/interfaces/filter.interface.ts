export interface Filter {
  od?: number;
  do?: number;
  datum?: string;
  voznje?: boolean;
  potraznje?: boolean;
}
