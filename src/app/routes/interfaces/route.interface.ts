import {Region} from '../../shared/region.interface';
import {RouteStatusType} from '../enums/route-status-type.enum';

export interface Route {
   id?: number;
   fromLocationName?: string;
   fromLocationId?: number;
   toLocationName?: string;
   toLocationId?: number;
   date?: any;
   price?: number;
   numOfSeats?: number;
   status?: RouteStatusType;
   isFeatured?: boolean;
   userId?: number;
   userUsername?: string;
   carId?: number;
   description?: string;
   startingPlace?: string;
   sublocations?: Region[];
   numberOfViews?: number;
   userRating?: number;
}
