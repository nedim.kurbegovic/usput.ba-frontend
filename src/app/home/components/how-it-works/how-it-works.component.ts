import { Component } from '@angular/core';
import {
  faPlus,
  faSearch,
  faSearchLocation,
} from '@fortawesome/free-solid-svg-icons';
import {
  faSadCry,
  faSadTear,
  faSmileBeam,
  faSmileWink,
} from '@fortawesome/free-regular-svg-icons';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons/faPlusCircle';
import { AppRoutes } from '../../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
})
export class HowItWorksComponent {
  steps = [
    {
      title: 'Trebaš prevoz?',
      description:
        'Pretraži objavljene vožnje i prijavi se na onu koja ti odgovara.',
      faIcon: faSmileBeam,
      button: {
        title: 'Vidi vožnje',
        faIcon: faSearch,
        routerLink: AppRoutes.Routes,
        queryParams: { voznje: 'da' },
      },
    },
    {
      title: 'Nudiš prevoz?',
      description:
        'Objavi vožnju i odobri/odbij prijave ljudi koji se na nju prijave.',
      faIcon: faSmileWink,
      button: {
        title: 'Objavi vožnju',
        faIcon: faPlus,
        routerLink: '',
      },
    },
    {
      title: 'Nema vožnji?',
      description:
        'Napravi potražnju i obavijestit ćemo te ako nekom vozaču to odgovara.',
      faIcon: faSadTear,
      button: {
        title: 'Napravi potražnju',
        faIcon: faPlusCircle,
        routerLink: '',
      },
    },
    {
      title: 'Nema putnika?',
      description:
        'Pregledaj potražnje i pretvori onu koja ti odgovara u svoju vožnju.',
      faIcon: faSadCry,
      button: {
        title: 'Vidi potražnje',
        faIcon: faSearchLocation,
        routerLink: AppRoutes.Routes,
        queryParams: { potraznje: 'da' },
      },
    },
  ];

  constructor() {}
}
