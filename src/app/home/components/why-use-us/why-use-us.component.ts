import {Component} from '@angular/core';

@Component({
  selector: 'app-why-use-us',
  templateUrl: './why-use-us.component.html'
})
export class WhyUseUsComponent {
  useCases = [
    {
      title: 'Sloboda',
      description: 'Ti biraš s kim ćeš se voziti, gdje i kada ćeš ići.'
    },
    {
      title: 'Sigurnost',
      description: 'Pregledaj dojmove i medalje na profilima vozača.'
    },
    {
      title: 'Ušteda',
      description: 'Uštedi i smanji troškove dijeljenjem prevoza.'
    },
    {
      title: 'Druženje',
      description: 'Upoznaj različite ljude i ostvari nove konekcije.'
    }
  ];

  constructor() { }

}
