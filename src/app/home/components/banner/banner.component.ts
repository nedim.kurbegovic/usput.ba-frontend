import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { getRegions } from '../../../state/app.selectors';
import { loadRegionsAction } from '../../../state/app.actions';
import { Observable } from 'rxjs';
import { Region } from '../../../shared/region.interface';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { AppRoutes } from '../../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.scss'],
})
export class BannerComponent {
  faSearch = faSearch;
  findOptions = ['prevoz 🚗', 'saputnike 👨‍👩‍👧‍👦', 'potražnju 📍', 'društvo 👫'];
  regions$: Observable<Region[]> = this.store.select(getRegions);
  selectedPolazisteId: number | undefined;
  selectedDestinacijaId: number | undefined;

  constructor(private store: Store, private router: Router) {
    this.store.dispatch(loadRegionsAction());
  }

  search(): void {
    const queryParams = {
      od: this.selectedPolazisteId,
      do: this.selectedDestinacijaId,
      voznje: 'da',
      potraznje: 'da',
    };
    this.router.navigate(['/' + AppRoutes.Routes], { queryParams });
  }
}
