import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeRoutingModule} from './home-routing.module';
import {HomeComponent} from './home.component';
import {BannerComponent} from './components/banner/banner.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {FormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {HowItWorksComponent} from './components/how-it-works/how-it-works.component';
import {WhyUseUsComponent} from './components/why-use-us/why-use-us.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    HomeComponent,
    BannerComponent,
    HowItWorksComponent,
    WhyUseUsComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    NgSelectModule,
    FormsModule,
    FontAwesomeModule,
    SharedModule
  ]
})
export class HomeModule { }
