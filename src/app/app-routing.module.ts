import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppRoutes } from './shared/enums/app-routes.enum';

const routes: Routes = [
  {
    path: AppRoutes.Home,
    loadChildren: () => import('./home/home.module').then((m) => m.HomeModule),
  },
  {
    path: AppRoutes.Profile,
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfileModule),
  },
  {
    path: AppRoutes.Chat,
    loadChildren: () => import('./chat/chat.module').then((m) => m.ChatModule),
  },
  {
    path: AppRoutes.Login,
    loadChildren: () => import('./auth/auth.module').then((m) => m.AuthModule),
  },
  {
    path: AppRoutes.Routes,
    loadChildren: () =>
      import('./routes/routes.module').then((m) => m.RoutesModule),
  },
  {
    path: '**',
    redirectTo: AppRoutes.Home,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
