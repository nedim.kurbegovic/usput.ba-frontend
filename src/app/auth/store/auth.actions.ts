import { createAction, props } from '@ngrx/store';
import { User } from '../../shared/interfaces/user.interface';
import { RestResponse } from '../../shared/interfaces/rest-response.interface';
import { ErrorBackend } from '../../shared/interfaces/error-backend.interface';

export const logInAction = createAction(
  '[Auth Actions] Log In',
  props<{ login: string; password: string }>()
);

export const logInSuccessAction = createAction(
  '[Auth Actions] Log In Success',
  props<{ user: User; isLoggedIn: boolean }>()
);

export const registerAction = createAction(
  '[Auth Actions] Register',
  props<{ user: User }>()
);

export const registerSuccessAction = createAction(
  '[Auth Actions] Register Success',
  props<{ user: User }>()
);

export const updateAction = createAction(
  '[Auth Actions] Update',
  props<{ user: User }>()
);

export const updateSuccessAction = createAction(
  '[Auth Actions] Update Success',
  props<{ user: User }>()
);

export const logOutAction = createAction('[Auth Actions] Log out');

export const resendVerificationEmailAction = createAction(
  '[Auth Actions] Resend Confirm Email',
  props<{ email: string }>()
);

export const verifyEmailTokenAction = createAction(
  '[Auth Actions] Verify Email Token',
  props<{ key: string }>()
);

export const verifyEmailTokenSuccessAction = createAction(
  '[Auth Actions] Verify Email Token Success',
  props<{ user: User }>()
);

export const verifyEmailTokenFailureAction = createAction(
  '[Auth Actions] Verify Email Token Failure',
  props<{ error: ErrorBackend }>()
);

export const sendPasswordForgotEmailAction = createAction(
  '[Auth Actions] Send Password Forgot',
  props<{ identifier: string }>()
);

export const sendPasswordForgotEmailSuccessAction = createAction(
  '[Auth Actions] Send Password Forgot Success',
  props<{ response: RestResponse }>()
);

export const recoverPasswordAction = createAction(
  '[Auth Actions] Change Password Action',
  props<{ newPassword: string; recoveryKey: string }>()
);

export const recoverPasswordSuccessAction = createAction(
  '[Auth Actions] Change Password Success Action',
  props<{ response: RestResponse }>()
);
