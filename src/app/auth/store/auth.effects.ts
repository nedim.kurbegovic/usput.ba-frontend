import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import {
  logInAction,
  logInSuccessAction,
  logOutAction,
  recoverPasswordAction,
  recoverPasswordSuccessAction,
  registerAction,
  registerSuccessAction,
  resendVerificationEmailAction,
  sendPasswordForgotEmailAction,
  sendPasswordForgotEmailSuccessAction,
  updateAction,
  updateSuccessAction,
  verifyEmailTokenAction,
  verifyEmailTokenFailureAction,
  verifyEmailTokenSuccessAction,
} from './auth.actions';
import { AuthService } from '../services/auth.service';
import { LocalStorageKeys } from '../../shared/constants/local-storage.keys';
import { Utils } from '../../shared/utils/utils';
import { HttpErrorResponse } from '@angular/common/http';
import { AuthRoutes } from '../enums/route.enum';
import { showToastAction } from '../../state/app.actions';
import { ToastErrorType } from '../../shared/enums/toast-error-type.enum';
import { ActivationService } from '../services/activation.service';
import { AppRoutes } from '../../shared/enums/app-routes.enum';
import { of } from 'rxjs';

@Injectable()
export class AuthEffects {
  logIn$ = createEffect(() =>
    this.actions$.pipe(
      ofType(logInAction),
      switchMap((action) =>
        this.service.login(action.login, action.password).pipe(
          map((loginResponse) => {
            if (loginResponse.user && loginResponse.user.isEmailVerified) {
              localStorage.setItem(LocalStorageKeys.Token, loginResponse.token);
              localStorage.setItem(
                LocalStorageKeys.RefreshToken,
                loginResponse.refreshToken
              );
              localStorage.setItem(
                LocalStorageKeys.User,
                JSON.stringify(loginResponse.user)
              );
              this.router.navigate([AppRoutes.Profile]);
              return logInSuccessAction({
                user: loginResponse.user,
                isLoggedIn: true,
              });
            } else {
              this.router.navigate([
                '/' + AppRoutes.Login + '/' + AuthRoutes.OneMoreStep,
              ]);
              return logInSuccessAction({
                user: { email: loginResponse.user.email },
                isLoggedIn: false,
              });
            }
          }),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        )
      )
    )
  );

  register$ = createEffect(() =>
    this.actions$.pipe(
      ofType(registerAction),
      switchMap((action) =>
        this.service.register(action.user).pipe(
          map((user) => registerSuccessAction({ user: { email: user.email } })),
          tap(() =>
            this.router.navigate([
              '/' + AppRoutes.Login + '/' + AuthRoutes.OneMoreStep,
            ])
          ),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        )
      )
    )
  );

  update$ = createEffect(() =>
    this.actions$.pipe(
      ofType(updateAction),
      switchMap((action) =>
        this.service.update(action.user).pipe(
          map((user) => updateSuccessAction({ user })),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        )
      )
    )
  );

  logOut$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(logOutAction),
        tap(() => {
          localStorage.removeItem(LocalStorageKeys.Token);
          localStorage.removeItem(LocalStorageKeys.RefreshToken);
          localStorage.removeItem(LocalStorageKeys.User);
          this.router.navigate(['/' + AuthRoutes.Login]);
        })
      ),
    { dispatch: false }
  );

  resendVerificationEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(resendVerificationEmailAction),
      switchMap((action) =>
        this.activation.resendVerificationEmail(action.email).pipe(
          map((response) =>
            showToastAction({
              toast: { type: ToastErrorType.Success, title: response.subject },
            })
          ),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        )
      )
    )
  );

  verifyTokenEmail$ = createEffect(() =>
    this.actions$.pipe(
      ofType(verifyEmailTokenAction),
      switchMap((action) =>
        this.activation.verifyTokenEmail(action.key).pipe(
          map((user) => verifyEmailTokenSuccessAction({ user })),
          catchError((error: HttpErrorResponse) =>
            of(verifyEmailTokenFailureAction({ error: error.error }))
          )
        )
      )
    )
  );

  sendForgotPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(sendPasswordForgotEmailAction),
      switchMap((action) =>
        this.activation.sendPasswordRecoveryEmail(action.identifier).pipe(
          map((response) => sendPasswordForgotEmailSuccessAction({ response })),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        )
      )
    )
  );

  recoverPassword$ = createEffect(() =>
    this.actions$.pipe(
      ofType(recoverPasswordAction),
      switchMap((action) => {
        return this.activation.isValid(action.recoveryKey).pipe(
          switchMap(() =>
            this.activation
              .setNew(action.newPassword, action.recoveryKey)
              .pipe(
                map((res) => recoverPasswordSuccessAction({ response: res }))
              )
          ),
          catchError((error: HttpErrorResponse) =>
            Utils.GetToastErrorObservable(error)
          )
        );
      })
    )
  );

  constructor(
    private router: Router,
    private actions$: Actions,
    private service: AuthService,
    private activation: ActivationService
  ) {}
}
