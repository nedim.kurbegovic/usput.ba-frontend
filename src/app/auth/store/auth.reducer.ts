import { Action, createReducer, on } from '@ngrx/store';
import { User } from '../../shared/interfaces/user.interface';
import {
  logInSuccessAction,
  logOutAction,
  registerSuccessAction,
  updateSuccessAction,
  verifyEmailTokenFailureAction,
  verifyEmailTokenSuccessAction,
} from './auth.actions';
import { ErrorBackend } from '../../shared/interfaces/error-backend.interface';
import { LocalStorageKeys } from '../../shared/constants/local-storage.keys';

export const authFeatureKey = 'auth';

export interface AuthState {
  currentUser: User | undefined;
  error: ErrorBackend | undefined;
  isLoggedIn: boolean;
}

export const initialState: AuthState = {
  currentUser: undefined,
  error: undefined,
  isLoggedIn: !!localStorage.getItem(LocalStorageKeys.User),
};

const authReducer = createReducer<AuthState>(
  initialState,
  on(logInSuccessAction, (state, { user, isLoggedIn }) => ({
    ...state,
    currentUser: user,
    isLoggedIn,
  })),
  on(registerSuccessAction, (state, { user }) => ({
    ...state,
    currentUser: user,
  })),
  on(updateSuccessAction, (state, { user }) => ({
    ...state,
    currentUser: user,
  })),
  on(logOutAction, (state) => ({
    ...state,
    currentUser: undefined,
    isLoggedIn: false,
  })),
  on(verifyEmailTokenSuccessAction, (state, { user }) => ({
    ...state,
    currentUser: user,
  })),
  on(verifyEmailTokenFailureAction, (state, { error }) => ({
    ...state,
    error,
  }))
);

export function reducer(state: AuthState, action: Action): AuthState {
  return authReducer(state, action);
}
