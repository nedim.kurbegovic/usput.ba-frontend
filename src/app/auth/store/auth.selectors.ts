import { createFeatureSelector, createSelector } from '@ngrx/store';
import { authFeatureKey, AuthState } from './auth.reducer';

export const getAuthState = createFeatureSelector<AuthState>(authFeatureKey);

export const getCurrentUser = createSelector(
  getAuthState,
  (state: AuthState) => state.currentUser
);

export const getIsUserLoggedIn = createSelector(
  getAuthState,
  (state: AuthState) => state?.isLoggedIn
);
