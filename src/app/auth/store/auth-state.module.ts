import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import * as fromAuth from './auth.reducer';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {authFeatureKey} from './auth.reducer';
import {AuthEffects} from './auth.effects';

@NgModule({
  imports: [
    StoreModule.forFeature(authFeatureKey, fromAuth.reducer),
    EffectsModule.forFeature([AuthEffects]),
    CommonModule
  ]
})
export class AuthStateModule {}
