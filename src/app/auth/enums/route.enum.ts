export enum AuthRoutes {
  Login = '',
  Register = 'registracija',
  ForgotPassword = 'zaboravljena-lozinka',
  ConfirmEmail = 'potvrdi-email',
  OneMoreStep = 'jos-jedan-korak',
}
