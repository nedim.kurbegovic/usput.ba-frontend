import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../../shared/interfaces/user.interface';
import {LoginResponse} from '../interfaces/login-response.interface';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private readonly URL = '/api';

  constructor(private http: HttpClient) {
  }

  login(login: string, password: string): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(this.URL + '/login', {username: login, password});
  }

  register(user: User): Observable<User> {
    return this.http.post<User>(this.URL + '/users', {...user, confirmPassword: user.password});
  }

  update(user: User): Observable<User> {
    return this.http.put<User>(this.URL + '/users', user);
  }

}
