import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RestResponse } from '../../shared/interfaces/rest-response.interface';
import { User } from '../../shared/interfaces/user.interface';

@Injectable({
  providedIn: 'root',
})
export class ActivationService {
  private readonly URL = '/api/activation';
  private readonly FORGOT_PASSWORD_URL = '/api/password-recovery';

  constructor(private http: HttpClient) {}

  public resendVerificationEmail(email: string): Observable<RestResponse> {
    return this.http.post<RestResponse>(this.URL + '/resend/' + email, null);
  }

  public verifyTokenEmail(activationKey: string): Observable<User> {
    return this.http.put<User>(this.URL + '/' + activationKey, null);
  }

  public sendPasswordRecoveryEmail(
    identifier: string
  ): Observable<RestResponse> {
    return this.http.post<RestResponse>(this.FORGOT_PASSWORD_URL, {
      email: identifier,
    });
  }

  public isValid(recoveryKey: string): Observable<{ keyValid: boolean }> {
    return this.http.get<{ keyValid: boolean }>(
      this.FORGOT_PASSWORD_URL + '/valid/' + recoveryKey
    );
  }

  public setNew(
    newPassword: string,
    recoveryKey: string
  ): Observable<RestResponse> {
    const passwordModel = {
      newPassword,
      confirmNewPassword: newPassword,
      recoveryKey,
    };
    return this.http.put<RestResponse>(
      this.FORGOT_PASSWORD_URL + '/recover-password',
      passwordModel
    );
  }
}
