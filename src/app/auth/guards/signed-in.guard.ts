import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { LocalStorageKeys } from '../../shared/constants/local-storage.keys';
import { AppRoutes } from '../../shared/enums/app-routes.enum';

@Injectable({
  providedIn: 'root',
})
export class SignedInGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(): boolean {
    if (!localStorage.getItem(LocalStorageKeys.User)) {
      this.router.navigate(['/' + AppRoutes.Login]);
      return false;
    }
    return true;
  }
}
