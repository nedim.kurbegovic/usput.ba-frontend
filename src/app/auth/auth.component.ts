import {Component} from '@angular/core';
import {fadeChildrenRouterOutletAnimation} from '../shared/animations/route.animations';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  animations: [fadeChildrenRouterOutletAnimation],
  styles: ['section { position: relative; display: block; height: 100% }']
})
export class AuthComponent {
  constructor() {}

}
