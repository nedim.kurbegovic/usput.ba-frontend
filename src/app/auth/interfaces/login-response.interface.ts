import {User} from '../../shared/interfaces/user.interface';

export interface LoginResponse {
  token: string;
  refreshToken: string;
  user: User;
}
