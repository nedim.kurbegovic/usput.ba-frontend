import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthRoutes} from '../../enums/route.enum';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {logInAction} from '../../store/auth.actions';
import {faUser} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../auth.component.scss']
})
export class LoginComponent {
  faUser = faUser;
  form: FormGroup;

  constructor(
    private store: Store,
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.formBuilder.group({
      email: [null, Validators.required],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  goToForgotPassword(): void {
    this.router.navigate([AuthRoutes.ForgotPassword], {relativeTo: this.activatedRoute});
  }

  goToRegister(): void {
    this.router.navigate([AuthRoutes.Register], {relativeTo: this.activatedRoute});
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const data = this.form.value;
      this.store.dispatch(logInAction({login: data.email, password: data.password}));
    }
  }
}
