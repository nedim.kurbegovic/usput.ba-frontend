import { Component, OnDestroy } from '@angular/core';
import { AppRoutes } from '../../../shared/enums/app-routes.enum';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import {
  verifyEmailTokenAction,
  verifyEmailTokenFailureAction,
  verifyEmailTokenSuccessAction,
} from '../../store/auth.actions';
import { takeUntil } from 'rxjs/operators';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['../../auth.component.scss'],
})
export class ConfirmEmailComponent implements OnDestroy {
  description: string | undefined;
  title: string | undefined = 'Učitavanje...';
  didActivate = false;
  private unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store,
    private router: Router,
    private actions$: Actions,
    private activatedRoute: ActivatedRoute
  ) {
    this.observeResponse();
    this.store.dispatch(
      verifyEmailTokenAction({ key: this.activatedRoute.snapshot.params.token })
    );
  }

  private observeResponse(): void {
    this.actions$
      .pipe(ofType(verifyEmailTokenSuccessAction), takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.user) {
          this.title = 'Super!';
          this.description = 'Email adresa je uspješno verifikovana.';
          this.didActivate = true;
        }
      });
    this.actions$
      .pipe(ofType(verifyEmailTokenFailureAction), takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.error) {
          this.title = 'Greška';
          this.description = res.error.title;
        }
      });
  }

  goToLogin(): void {
    this.router.navigate(['/' + AppRoutes.Login]);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
