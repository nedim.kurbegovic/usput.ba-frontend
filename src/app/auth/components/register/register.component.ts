import {Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {faIdBadge, faUser, faUserTie} from '@fortawesome/free-solid-svg-icons';
import {AppRoutes} from '../../../shared/enums/app-routes.enum';
import {Store} from '@ngrx/store';
import {Gender} from '../../../shared/enums/gender.enum';
import {registerAction} from '../../store/auth.actions';
import {Utils} from '../../../shared/utils/utils';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../../auth.component.scss']
})
export class RegisterComponent {
  faUser = faUser;
  faIdBadge = faIdBadge;
  faUserTie = faUserTie;
  form: FormGroup;

  genderOptions = [
    {
      text: 'Muško',
      value: Gender.MALE
    },
    {
      text: 'Žensko',
      value: Gender.FEMALE
    }
  ];

  constructor(
    private store: Store,
    private router: Router,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      gender: [null, Validators.required],
      birthday: [null, Validators.required],
      lastName: [null, Validators.required],
      firstName: [null, Validators.required],
      email: [null, Validators.compose([Validators.required, Validators.email])],
      username: [null, Validators.compose([Validators.required, Validators.minLength(6)])],
      password: [null, Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

  goToLogin(): void {
    this.router.navigate(['/' + AppRoutes.Login]);
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      const user = this.form.value;
      this.store.dispatch(registerAction({user: {...user, birthday: Utils.GetIsoDateFromNgbDatepicker(user.birthday)}}));
    }
  }
}
