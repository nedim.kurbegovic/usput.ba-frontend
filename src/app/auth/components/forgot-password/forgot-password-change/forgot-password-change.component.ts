import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  recoverPasswordAction,
  recoverPasswordSuccessAction,
} from '../../../store/auth.actions';
import { Subject } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil } from 'rxjs/operators';
import { AppRoutes } from '../../../../shared/enums/app-routes.enum';

@Component({
  selector: 'app-forgot-password-change',
  templateUrl: './forgot-password-change.component.html',
  styleUrls: ['../../../auth.component.scss'],
})
export class ForgotPasswordChangeComponent implements OnDestroy {
  isChanged = false;
  faCheck = faCheck;
  form: FormGroup;
  title = 'Nova lozinka';
  description =
    'Drago nam je da te vidimo opet. Unesi svoju novu lozinku ispod:';
  private unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store,
    private router: Router,
    private actions$: Actions,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute
  ) {
    this.form = this.formBuilder.group({
      password: [
        null,
        Validators.compose([Validators.required, Validators.minLength(6)]),
      ],
    });
    this.observeSuccess();
  }

  private observeSuccess(): void {
    this.actions$
      .pipe(ofType(recoverPasswordSuccessAction), takeUntil(this.unsubscribe$))
      .subscribe((res) => {
        if (res.response) {
          this.isChanged = true;
          this.title = 'Super!';
          this.description =
            'Lozinka je promijenjena. Sada se možeš prijaviti u svoj račun.';
        }
      });
  }

  submit(): void {
    if (this.form.valid) {
      const data = this.form.value;
      this.store.dispatch(
        recoverPasswordAction({
          newPassword: data.password,
          recoveryKey: this.activatedRoute.snapshot.params.token,
        })
      );
    }
  }

  goToLogin(): void {
    this.router.navigate(['/' + AppRoutes.Login]);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
