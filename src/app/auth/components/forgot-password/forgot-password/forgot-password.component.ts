import { Component, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { faCheck, faUser } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import {
  sendPasswordForgotEmailAction,
  sendPasswordForgotEmailSuccessAction,
} from '../../../store/auth.actions';
import { Subject } from 'rxjs';
import { Actions, ofType } from '@ngrx/effects';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['../../../auth.component.scss'],
})
export class ForgotPasswordComponent implements OnDestroy {
  form: FormGroup;
  faUser = faUser;
  faCheck = faCheck;
  didSendEmail = false;
  title = 'Zaboravljena lozinka?';
  description =
    'Unesi svoju email adresu ili korisničko ime ispod i poslaćemo ti link za postavljanje nove lozinke.';

  private unsubscribe$ = new Subject<void>();

  constructor(
    private store: Store,
    private actions$: Actions,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      identifier: [null, Validators.required],
    });
    this.observeForgotPasswordSuccess();
  }

  private observeForgotPasswordSuccess(): void {
    this.actions$
      .pipe(
        ofType(sendPasswordForgotEmailSuccessAction),
        takeUntil(this.unsubscribe$)
      )
      .subscribe((res) => {
        this.title = res.response?.message
          ? res.response.message
          : 'Zahtjev poslan!';
        this.description = res.response?.subject
          ? res.response.subject
          : 'Email uskoro stiže. Ne zaboravi provjeriti Spam/Neželjenu poštu.';
        this.didSendEmail = true;
      });
  }

  submit(): void {
    this.form.markAllAsTouched();
    if (this.form.valid) {
      this.store.dispatch(
        sendPasswordForgotEmailAction({
          identifier: this.form.value.identifier,
        })
      );
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
