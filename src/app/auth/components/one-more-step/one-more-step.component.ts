import { Component, OnDestroy } from '@angular/core';
import { faSync } from '@fortawesome/free-solid-svg-icons';
import { Store } from '@ngrx/store';
import { CONFIRMATION_EMAIL_RESEND_NUMBER_OF_SECONDS } from '../../../shared/constants/constants';
import { resendVerificationEmailAction } from '../../store/auth.actions';
import { Subject } from 'rxjs';
import { getCurrentUser } from '../../store/auth.selectors';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-one-more-step',
  templateUrl: './one-more-step.component.html',
  styleUrls: ['../../auth.component.scss'],
})
export class OneMoreStepComponent implements OnDestroy {
  faSync = faSync;
  didNotArrive = false;
  secondsLeftToResend = 0;
  interval: any;

  private unsubscribe$ = new Subject<void>();

  constructor(private store: Store) {}

  resend(): void {
    if (this.secondsLeftToResend === 0) {
      clearInterval(this.interval);
      this.startResendEmailTimer();
    }

    this.store
      .select(getCurrentUser)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((user) => {
        if (user) {
          this.store.dispatch(
            resendVerificationEmailAction({ email: user.email })
          );
        }
      });
  }

  private startResendEmailTimer(): void {
    this.secondsLeftToResend = CONFIRMATION_EMAIL_RESEND_NUMBER_OF_SECONDS;
    this.interval = setInterval(() => {
      if (this.secondsLeftToResend > 0) {
        this.secondsLeftToResend -= 1;
      } else {
        this.secondsLeftToResend = 0;
      }
    }, 1000);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
