import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './components/confirm-email/confirm-email.component';
import { AuthRoutes } from './enums/route.enum';
import { ForgotPasswordChangeComponent } from './components/forgot-password/forgot-password-change/forgot-password-change.component';
import { OneMoreStepComponent } from './components/one-more-step/one-more-step.component';
import { NoDirectAccessGuard } from './guards/no-direct-access.guard';
import { NotSignedInGuard } from './guards/not-signed-in.guard';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent,
    children: [
      {
        path: '',
        component: LoginComponent,
        canActivate: [NotSignedInGuard],
      },
      {
        path: AuthRoutes.Register,
        component: RegisterComponent,
        canActivate: [NotSignedInGuard],
      },
      {
        path: AuthRoutes.ForgotPassword,
        component: ForgotPasswordComponent,
        canActivate: [NotSignedInGuard],
      },
      {
        path: AuthRoutes.ForgotPassword + '/:token',
        component: ForgotPasswordChangeComponent,
      },
      {
        path: AuthRoutes.ConfirmEmail + '/:token',
        component: ConfirmEmailComponent,
      },
      {
        path: AuthRoutes.OneMoreStep,
        component: OneMoreStepComponent,
        canActivate: [NoDirectAccessGuard],
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRoutingModule {}
