import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
import { AuthComponent } from './auth.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password/forgot-password.component';
import { ConfirmEmailComponent } from './components/confirm-email/confirm-email.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ForgotPasswordChangeComponent } from './components/forgot-password/forgot-password-change/forgot-password-change.component';
import { OneMoreStepComponent } from './components/one-more-step/one-more-step.component';

@NgModule({
  declarations: [
    AuthComponent,
    ForgotPasswordComponent,
    ConfirmEmailComponent,
    LoginComponent,
    RegisterComponent,
    ForgotPasswordChangeComponent,
    OneMoreStepComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
  ],
})
export class AuthModule {}
