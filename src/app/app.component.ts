import {Component} from '@angular/core';
import {NgSelectConfig} from '@ng-select/ng-select';
import {Store} from '@ngrx/store';
import {getToast} from './state/app.selectors';
import {getSpinnerCounter} from './layout/spinner/store/spinner.selectors';
import {ToastrService} from 'ngx-toastr';
import {Toast} from './shared/interfaces/toast.interface';
import {fadeAnimation} from './shared/animations/route.animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  animations: [fadeAnimation],
  styles: ['main { height: 100%; }']
})
export class AppComponent {
  timer: any = null;
  showSpinner: boolean | undefined;
  toastOptions = {
    positionClass: 'toast-bottom-right'
  };

  constructor(
    private store: Store,
    private toaster: ToastrService,
    private config: NgSelectConfig,
  ) {
    this.configureNgSelect();
    this.observeToasts();
    this.observeSpinnerActivation();
  }

  private observeSpinnerActivation(): void {
    this.store.select(getSpinnerCounter).subscribe((state) => {
      clearTimeout(this.timer);
      this.timer = setTimeout(() => {
        this.showSpinner = state?.counter !== 0;
      }, 0);
    });
  }

  private configureNgSelect(): void {
    this.config.notFoundText = 'Nema rezultata';
    this.config.appendTo = 'body';
  }

  private observeToasts(): void {
    this.store.select(getToast).subscribe((toast) => {
      if (!toast) {
        return;
      }
      this.showToast(toast);
    });
  }

  private showToast(toast: Toast): void {
    switch (toast.type) {
      case 'success':
        this.toaster.success(toast.title ? toast.title : 'Uspjeh', undefined, this.toastOptions);
        break;
      case 'error':
        this.toaster.error(toast.title ? toast.title : 'Došlo je do nepoznate greške.', undefined, this.toastOptions);
        break;
      case 'warning':
        this.toaster.warning(toast.title ? toast.title : 'Došlo je do nepoznatog problema.', undefined, this.toastOptions);
        break;
      default:
        this.toaster.info(toast.title, undefined, this.toastOptions);
    }
  }
}
