import {Action, createReducer, on} from '@ngrx/store';
import {Toast} from '../shared/interfaces/toast.interface';
import {loadRegionsSuccessAction, showToastAction} from './app.actions';
import {Region} from '../shared/region.interface';

export const appFeatureKey = 'app';

export interface AppState {
  toast: Toast | undefined;
  regions: Region[];
}

export const initialState: AppState = {
  toast: undefined,
  regions: []
};

const appReducer = createReducer<AppState>(
  initialState,
  on(showToastAction, (state, { toast }) => ({
    ...state,
    toast,
  })),
  on(loadRegionsSuccessAction, (state, { regions }) => ({
    ...state,
    regions
  }))
);

export function reducer(state: AppState, action: Action): AppState {
  return appReducer(state, action);
}
