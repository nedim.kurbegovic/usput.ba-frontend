import {createAction, props} from '@ngrx/store';
import {Toast} from '../shared/interfaces/toast.interface';
import {Region} from '../shared/region.interface';

export const showToastAction = createAction(
  '[App Actions] Show Toast Action',
  props<{ toast: Toast }>()
);

export const loadRegionsAction = createAction(
  '[App Action] Load Regions'
);

export const loadRegionsSuccessAction = createAction(
  '[App Action] Load Regions Success',
  props<{ regions: Region[] }>()
);
