import {createFeatureSelector, createSelector} from '@ngrx/store';
import {appFeatureKey, AppState} from './app.reducer';

export const getAppState = createFeatureSelector<AppState>(appFeatureKey);

export const getToast = createSelector(
  getAppState, (state: AppState) => state.toast
);

export const getRegions = createSelector(
  getAppState, (state: AppState) => state.regions
);
