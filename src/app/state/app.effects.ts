import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {routesFailureAction} from '../routes/store/routes.actions';
import {catchError, map, switchMap} from 'rxjs/operators';
import {of} from 'rxjs';
import {Router} from '@angular/router';
import {loadRegionsAction, loadRegionsSuccessAction} from './app.actions';
import {RegionsService} from '../shared/services/regions.service';

@Injectable()
export class AppEffects {
  loadRegions$ = createEffect(() => this.actions$.pipe(
    ofType(loadRegionsAction),
    switchMap((action) => {
      return this.service.get().pipe(
        map((res) => {
          return loadRegionsSuccessAction({
            regions: res,
          });
        }),
        catchError((error) => {
          return of(routesFailureAction({error}));
        })
      );
    })
  ));

  constructor(
    private actions$: Actions,
    private router: Router,
    private service: RegionsService
  ) {
  }
}
