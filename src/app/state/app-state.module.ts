import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import * as fromApp from './app.reducer';
import {CommonModule} from '@angular/common';
import {appFeatureKey} from './app.reducer';
import {EffectsModule} from '@ngrx/effects';
import {AppEffects} from './app.effects';

@NgModule({
  imports: [
    StoreModule.forFeature(appFeatureKey, fromApp.reducer),
    EffectsModule.forFeature([AppEffects]),
    CommonModule
  ]
})
export class AppStateModule {
}
