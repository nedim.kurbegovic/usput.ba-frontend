import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './layout/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SharedModule } from './shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { AppStateModule } from './state/app-state.module';
import { provideSpinnerInterceptor } from './shared/interceptors/spinner.interceptor';
import { provideNgbDatePickerAdapter } from './shared/adapters/ngb-date-picker.adapter';
import { provideNgbDateParserAdapter } from './shared/adapters/ngb-date-parser.adapter';
import { SpinnerModule } from './layout/spinner/spinner.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { provideTokenInterceptor } from './shared/interceptors/token.interceptor';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { provideBosnianDatepicker } from './shared/adapters/ngb-date-picker-localization';
import { AuthStateModule } from './auth/store/auth-state.module';

@NgModule({
  declarations: [AppComponent, HeaderComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule,
    SharedModule,
    HttpClientModule,
    SpinnerModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    StoreModule.forRoot(
      {},
      {
        metaReducers: !environment.production ? [] : [],
        runtimeChecks: {
          strictActionImmutability: true,
          strictStateImmutability: true,
        },
      }
    ),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    AppStateModule,
    AuthStateModule,
  ],
  providers: [
    provideNgbDateParserAdapter(),
    provideNgbDatePickerAdapter(),
    provideSpinnerInterceptor(),
    provideBosnianDatepicker(),
    provideTokenInterceptor(),
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
