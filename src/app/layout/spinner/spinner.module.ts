import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SpinnerComponent} from './spinner.component';
import {SpinnerStateModule} from './store/spinner-state.module';

@NgModule({
  declarations: [SpinnerComponent],
  imports: [CommonModule, SpinnerStateModule],
  exports: [SpinnerComponent]
})
export class SpinnerModule {
}
