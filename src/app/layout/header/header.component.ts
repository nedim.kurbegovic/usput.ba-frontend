import { Component, HostListener } from '@angular/core';
import { faEye, faPlusCircle, faUser } from '@fortawesome/free-solid-svg-icons';
import { AppRoutes } from '../../shared/enums/app-routes.enum';
import { AuthRoutes } from '../../auth/enums/route.enum';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { HeaderMenuItem } from '../../shared/interfaces/header-menu-item.interface';
import { logOutAction } from '../../auth/store/auth.actions';
import { getIsUserLoggedIn } from '../../auth/store/auth.selectors';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent {
  faEye = faEye;
  faUser = faUser;
  faPlusCircle = faPlusCircle;

  scrolled = false;
  isNavbarExpanded = false;
  RoutesRoute = AppRoutes.Routes;
  isUserLoggedIn$ = this.store.select(getIsUserLoggedIn);

  profileLinks: HeaderMenuItem[] = [
    {
      title: 'Prijavi se',
      navigate: '/' + AppRoutes.Login,
    },
    {
      title: 'Napravi račun',
      navigate: '/' + AppRoutes.Login + '/' + AuthRoutes.Register,
    },
  ];
  signedInLinks: HeaderMenuItem[] = [
    {
      title: 'Moj profil',
      navigate: '/' + AppRoutes.Profile,
    },
    {
      title: 'Odjavi se',
      logOut: true,
    },
  ];

  constructor(private store: Store, private router: Router) {
    this.closeNavbarOnMobile();
  }

  closeNavbarOnMobile(): void {
    if (this.isNavbarExpanded) {
      this.isNavbarExpanded = false;
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll(): void {
    this.scrolled = window.pageYOffset > 88;
  }

  profileLinkClicked(event: any, item: HeaderMenuItem): void {
    event.preventDefault();
    this.closeNavbarOnMobile();
    if (item.navigate) {
      this.router.navigate([item.navigate]);
    } else if (item.logOut) {
      this.store.dispatch(logOutAction());
    }
  }
}
