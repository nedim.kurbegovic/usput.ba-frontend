import {ErrorBackend} from '../interfaces/error-backend.interface';
import {Observable, of} from 'rxjs';
import {showToastAction} from '../../state/app.actions';
import {ToastErrorType} from '../enums/toast-error-type.enum';
import {HttpErrorResponse} from '@angular/common/http';

export class Utils {
  public static GetToastErrorObservable(errorResponse: HttpErrorResponse): Observable<any> {
    const error: ErrorBackend = errorResponse.error;
    return of(
      showToastAction({toast: {title: error.title, message: error.message, status: error.status, type: ToastErrorType.Error}})
    );
  }

  public static GetIsoDateFromNgbDatepicker(value: string): string {
    const splitDate = value.split('.');
    return new Date(Number(splitDate[2]), Number(splitDate[1]) - 1, Number(splitDate[0]), 15).toISOString();
  }
}
