export interface HeaderMenuItem {
  title: string;
  navigate?: string;
  logOut?: boolean;
}
