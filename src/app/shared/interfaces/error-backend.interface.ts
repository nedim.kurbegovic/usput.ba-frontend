export interface ErrorBackend {
   entityName: string;
   errorKey: string;
   message: string;
   params: string;
   status: number;
   title: string;
   type: string;
}
