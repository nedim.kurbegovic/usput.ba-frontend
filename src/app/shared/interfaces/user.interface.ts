import {Role} from '../../auth/interfaces/role.interface';
import {Gender} from '../enums/gender.enum';

export interface User {
   id?: number;
   email: string;
   username?: string;
   firstName?: string;
   password?: string;
   confirmPassword?: string;
   lastName?: string;
   birthday?: Date;
   biography?: string;
   profileImageBlob?: any;
   profileImageContentType?: string;
   phoneNumber?: any;
   role?: Role[];
   regionName?: any;
   regionId?: any;
   isFeatured?: any;
   address?: string;
   joinedDate?: Date;
   facebookURL?: string;
   instagramURL?: string;
   ukreditiAmount?: number;
   isEmailVerified?: boolean;
   gender?: Gender;
   profileImageUrl?: string;
   isBanned?: boolean;
   reviewScore?: number;
   phoneNumberHidden?: boolean;
   addressHidden?: boolean;
   birthdayHidden?: boolean;
   facebookId?: string;
}
