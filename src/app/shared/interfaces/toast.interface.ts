import {ToastErrorType} from '../enums/toast-error-type.enum';

export interface Toast {
  title?: string;
  message?: string;
  type?: ToastErrorType;
  status?: number;
}
