export interface RestResponse {
  message?: string;
  subject?: string;
}
