import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { EmailInputComponent } from './components/inputs/email-input/email-input.component';
import { PasswordInputComponent } from './components/inputs/password-input/password-input.component';
import { BackButtonComponent } from './components/buttons/back-button/back-button.component';
import { RouterModule } from '@angular/router';
import { TextInputComponent } from './components/inputs/text-input/text-input.component';
import { DateInputComponent } from './components/inputs/date-input/date-input.component';
import { RadioInputComponent } from './components/inputs/radio-input/radio-input.component';
import { NgbDatepickerModule } from '@ng-bootstrap/ng-bootstrap';
import { ButtonComponent } from './components/buttons/button/button.component';
import { ToggleSwitchComponent } from './components/inputs/toggle-switch/toggle-switch.component';

const components = [
  FooterComponent,
  EmailInputComponent,
  PasswordInputComponent,
  BackButtonComponent,
  TextInputComponent,
  DateInputComponent,
  RadioInputComponent,
  ButtonComponent,
  ToggleSwitchComponent,
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    RouterModule,
    NgbDatepickerModule,
  ],
  exports: [...components],
})
export class SharedModule {}
