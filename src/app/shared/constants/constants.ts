export const SEARCH_RESULTS_SIZE = 20;
export const CONFIRMATION_EMAIL_RESEND_NUMBER_OF_SECONDS = 30;
export const MIN_SEARCH_DATE = {
  year: new Date().getFullYear(),
  month: new Date().getMonth() + 1,
  day: new Date().getDate(),
};
export const MAX_SEARCH_DATE = {
  year: new Date().getFullYear() + 1,
  month: new Date().getMonth() + 1,
  day: new Date().getDate(),
};
