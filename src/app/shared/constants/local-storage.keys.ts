export enum LocalStorageKeys {
  Token = 'token',
  RefreshToken = 'refreshToken',
  User = 'user',
}
