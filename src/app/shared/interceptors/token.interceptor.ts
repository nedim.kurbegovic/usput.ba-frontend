import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {LocalStorageKeys} from '../constants/local-storage.keys';
import {tap} from 'rxjs/operators';
import {Store} from '@ngrx/store';
import {logOutAction} from '../../auth/store/auth.actions';
import {showToastAction} from '../../state/app.actions';
import {ToastErrorType} from '../enums/toast-error-type.enum';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private store: Store) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = localStorage.getItem(LocalStorageKeys.Token);
    if (!token) {
      return this.authorizationAction(req, next);
    }
    req = req.clone({
      setHeaders: {
        Authorization: token
      }
    });
    return this.authorizationAction(req, next);
  }


  private authorizationAction(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).pipe(
      tap((event: HttpEvent<any>) => {
        },
        (err: any) => {
          if (err instanceof HttpErrorResponse) {
            if (err.status === 401) {
              this.store.dispatch(logOutAction());
              this.store.dispatch(
                showToastAction({toast: {type: ToastErrorType.Error, title: 'Token je istekao. Prijavi se ponovo.'}})
              );
            }
          }
        })
    );
  }
}

export const provideTokenInterceptor = () => ({
  provide: HTTP_INTERCEPTORS,
  useClass: TokenInterceptorService,
  multi: true
});
