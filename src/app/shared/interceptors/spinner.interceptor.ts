import {Injectable} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Store} from '@ngrx/store';
import {hideSpinnerAction, showSpinnerAction} from '../../layout/spinner/store/spinner.actions';
import {finalize} from 'rxjs/operators';
import {noSpinnerUrls} from '../constants/no-spinner-routes';

@Injectable({
  providedIn: 'root'
})
export class SpinnerInterceptorService implements HttpInterceptor {
  constructor(private store: Store) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!noSpinnerUrls.includes(req.url)) {
      this.showSpinner();
      return next.handle(req).pipe(finalize(() => this.onEnd()));
    }
    return next.handle(req);
  }

  private onEnd(): void {
    this.hideSpinner();
  }

  private showSpinner(): void {
    this.store.dispatch(showSpinnerAction());
  }

  private hideSpinner(): void {
    this.store.dispatch(hideSpinnerAction());
  }
}

export const provideSpinnerInterceptor = () => ({
    provide: HTTP_INTERCEPTORS,
    useClass: SpinnerInterceptorService,
    multi: true
  }
);
