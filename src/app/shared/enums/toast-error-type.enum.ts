export enum ToastErrorType {
  Error = 'error',
  Success = 'success',
  Info = 'info',
  Warning = 'warning'
}
