export enum AppRoutes {
  Home = '',
  Admin = 'admin',
  Profile = 'profil',
  Chat = 'chat',
  Login = 'prijava',
  Routes = 'voznje',
}
