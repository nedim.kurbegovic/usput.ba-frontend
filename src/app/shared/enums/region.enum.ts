export enum RegionType {
  City = 'CITY',
  Settlement = 'SETTLEMENT'
}
