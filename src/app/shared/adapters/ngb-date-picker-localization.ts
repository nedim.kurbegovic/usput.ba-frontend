import { Injectable } from '@angular/core';
import { NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class CustomBosnianDatepickerI18n extends NgbDatepickerI18n {
  constructor() {
    super();
  }

  getWeekdayShortName(weekday: number): string {
    return ['Ne', 'Po', 'Ut', 'Sr', 'Če', 'Pe', 'Su'][weekday - 1];
  }

  getMonthShortName(month: number): string {
    return [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'Maj',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Okt',
      'Nov',
      'Dec',
    ][month - 1];
  }

  getMonthFullName(month: number): string {
    return this.getMonthShortName(month);
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}

export const provideBosnianDatepicker = () => ({
  provide: NgbDatepickerI18n,
  useClass: CustomBosnianDatepickerI18n,
});
