import {RegionType} from './enums/region.enum';

export interface Region {
   name?: string;
   idParent?: number;
   id?: number;
   type?: RegionType;
   idParentName?: string;
}
