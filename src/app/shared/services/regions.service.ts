import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Region} from '../region.interface';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RegionsService {
  private readonly URL = '/api/regions';

  constructor(private http: HttpClient) {
  }

  get(): Observable<Region[]> {
    return this.http.get <Region[]>(this.URL + '/all');
  }
}
