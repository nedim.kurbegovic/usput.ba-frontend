import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['button.component.scss'],
})
export class ButtonComponent {
  @Input() value = '';
  @Input() faIcon: any;
  @Input() classList = '';
  @Input() routeLink: any;
  @Input() queryParams: any;
  @Input() disabled = false;
  @Output() clicked = new EventEmitter<void>();

  constructor(private router: Router) {}

  click(): void {
    this.clicked.emit();
    if (this.routeLink) {
      this.router.navigate([this.routeLink], { queryParams: this.queryParams });
    }
  }
}
