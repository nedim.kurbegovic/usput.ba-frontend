import {Component, Input} from '@angular/core';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-back-button',
  templateUrl: './back-button.component.html'
})
export class BackButtonComponent {
  @Input() cssClass = '';
  faArrowLeft = faArrowLeft;

  constructor() {
  }
}
