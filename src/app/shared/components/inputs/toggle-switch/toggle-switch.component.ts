import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-toggle-switch',
  templateUrl: 'toggle-switch.component.html',
  styleUrls: ['toggle-switch.component.scss'],
})
export class ToggleSwitchComponent {
  @Input() parentForm: any;
  @Input() controlName = '';
  @Input() label: string | undefined;
  @Output() changed = new EventEmitter<boolean>();

  toggled(event: Event): void {
    if (event && event.target) {
      const input = event.target as HTMLInputElement;
      this.changed.emit(input.checked);
    }
  }
}
