import { Component, Input, Output, EventEmitter } from '@angular/core';
import { faCalendar } from '@fortawesome/free-solid-svg-icons';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
})
export class DateInputComponent {
  @Input() parentForm: any;
  @Input() controlName = '';
  @Input() showPrependIcon = true;
  @Input() label: string | undefined;
  @Input() classList: string | undefined;
  @Input() placeholder: string | undefined;
  @Input() minDate: NgbDateStruct = { day: 15, month: 11, year: 1920 };
  @Input() maxDate: NgbDateStruct = { day: 15, month: 11, year: 2006 };
  @Output() dateSelected = new EventEmitter<string>();
  faCalendar = faCalendar;

  select(date?: NgbDateStruct): void {
    let dateToReturn = '';
    if (!date) {
      // Enter pressed
      if (this.parentForm.controls.datum.errors) {
        return;
      }
      dateToReturn = this.parentForm.value.datum;
    } else {
      dateToReturn =
        date.day.toString() +
        '.' +
        date.month.toString() +
        '.' +
        date.year.toString();
    }
    this.dateSelected.emit(dateToReturn);
  }
}
