import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-text-input',
  templateUrl: './text-input.component.html'
})
export class TextInputComponent {
  @Input() label: string | undefined;
  @Input() placeholder = '';
  @Input() controlName = '';
  @Input() parentForm: any;
  @Input() faIcon: any;

  constructor() {
  }
}
