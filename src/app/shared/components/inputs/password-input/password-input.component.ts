import {Component, Input, Output, EventEmitter} from '@angular/core';
import {faKey} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-password-input',
  templateUrl: './password-input.component.html'
})
export class PasswordInputComponent {
  @Input() parentForm: any;
  @Input() placeholder = '';
  @Input() controlName = '';
  @Input() label: string | undefined;
  faKey = faKey;

  @Output() didSubmit = new EventEmitter<void>();

  constructor() {
  }
}
