import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-radio-input',
  templateUrl: './radio-input.component.html'
})
export class RadioInputComponent {
  @Input() parentForm: any;
  @Input() controlName = '';
  @Input() label: string | undefined;
  @Input() radioOptions: { text: string, value: string }[] = [];

  constructor() {
  }
}
