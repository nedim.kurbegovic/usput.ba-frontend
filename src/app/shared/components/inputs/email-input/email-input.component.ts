import {Component, Input} from '@angular/core';
import {faEnvelope} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-email-input',
  templateUrl: './email-input.component.html'
})
export class EmailInputComponent {
  @Input() parentForm: any;
  @Input() label: string | undefined;
  faEnvelope = faEnvelope;

  constructor() {}
}
