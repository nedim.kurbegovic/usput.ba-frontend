export const environment = {
  production: true,
  apiURL: 'https://usput.ba',
  socketURL: 'wss://usput.ba/socket/websocket',
};
